/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huawei.housekeeper.dao.entity.Message;

/**
 * 功能描述
 *
 * @since 2022-02-15
 */
@Mapper
public interface MessageMapper extends BaseMapper<Message> {

    /**
     * 获取未读消息数量
     *
     * @param uid uid
     * @return 未读消息数量
     */
    int getMsgCount(String uid);

    /**
     * 修改消息状态
     *
     * @param idList 消息id
     * @param status 状态
     * @return 修改是否成功
     */
    boolean setMsgStatus(List<String> idList, Integer status);
}
