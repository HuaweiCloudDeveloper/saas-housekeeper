/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 用户登录
 *
 * @since 2022-02-08
 */
@Setter
@Getter
@ApiModel("用户登录")
public class UserLoginDto {
    @NotBlank(message = "用户名必填")
    @Length(min = 2, max = 10, message = "用户名长度范围: 2-10")
    @ApiModelProperty(value = "用户名", required = true)
    private String userName;

    @NotBlank(message = "必填")
    @Length(min = 6, max = 18, message = "长度范围: 6-18")
    @ApiModelProperty(value = "密码", required = true)
    private String password;

    @Min(value = 1, message = "最小值:1")
    @Max(value = 3, message = "最大值:3")
    @ApiModelProperty(value = "1：用户登录,2: 工人登录,3: 租户登录", required = false)
    private Integer accountType;
}
