/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * baseEntity
 *
 * @author l84165417
 * * @since  2022/1/26 17:28
 */
@Setter
@Getter
public class BaseEntity {
    /**
     * 业务主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME", updateStrategy = FieldStrategy.NEVER, fill = FieldFill.INSERT)
    private Date createdTime;

    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY", updateStrategy = FieldStrategy.NEVER, fill = FieldFill.INSERT)
    private String createdBy;

    /**
     * 修改时间
     */
    @TableField(value = "UPDATED_TIME", fill = FieldFill.INSERT_UPDATE)
    private Date updatedTime;

    /**
     * 修改人
     */
    @TableField(value = "UPDATED_BY", fill = FieldFill.INSERT_UPDATE)
    private String updatedBy;
}