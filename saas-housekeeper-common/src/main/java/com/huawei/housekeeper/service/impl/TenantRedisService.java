package com.huawei.housekeeper.service.impl;

import com.huawei.housekeeper.service.IRedisService;
import com.huawei.saashousekeeper.context.TenantContext;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * 租户使用的缓存服务，key加上前缀，方便数据隔离，避免使用数据库租户隔离时key混淆
 * @param <T>
 */
public class TenantRedisService<T> implements IRedisService<T> {
    @Resource
    private RedisTemplate<String,T> redisTemplate;

    private final static long DEFAULT_TIME = 10000L;

    private String tenantKey(String key){
        return TenantContext.getDomain() + key;
    }

    @Override
    public void set(String key, T value, long time) {
        redisTemplate.opsForValue().set(tenantKey(key),value,time, TimeUnit.SECONDS);
    }

    @Override
    public void set(String key, T value) {
        redisTemplate.opsForValue().set(tenantKey(key),value,DEFAULT_TIME,TimeUnit.SECONDS);
    }

    @Override
    public T get(String key) {
        return redisTemplate.opsForValue().get(tenantKey(key));
    }

    @Override
    public Boolean del(String key) {
        return redisTemplate.delete(tenantKey(key));
    }

    @Override
    public Long del(List<String> keys) {
        List<String> tenantKeys = new ArrayList<String>();
        keys.forEach( key ->
                tenantKeys.add(tenantKey(key))
        );
        return redisTemplate.delete(tenantKeys);
    }

    @Override
    public Boolean expire(String key, long time) {
        return redisTemplate.expire(tenantKey(key),time,TimeUnit.SECONDS);
    }

    @Override
    public Long getExpire(String key) {
        return redisTemplate.getExpire(tenantKey(key));
    }

    @Override
    public Boolean hasKey(String key) {
        return redisTemplate.hasKey(tenantKey(key));
    }

    @Override
    public Long incr(String key, long delta) {
        return redisTemplate.opsForValue().increment(tenantKey(key),delta);
    }

    @Override
    public Long decr(String key, long delta) {
        return redisTemplate.opsForValue().increment(tenantKey(key),-delta);
    }

    @Override
    public T hGet(String key, String hashKey) {
        return (T) redisTemplate.opsForHash().get(tenantKey(key),hashKey);
    }

    @Override
    public Boolean hSet(String key, String hashKey, T value, long time) {
        redisTemplate.opsForHash().put(tenantKey(key),hashKey,value);
        return expire(key,time);
    }

    @Override
    public void hSet(String key, String hashKey, T value) {
        redisTemplate.opsForHash().put(tenantKey(key),hashKey,value);
    }

    @Override
    public Map<Object, T> hGetAll(String key) {
        return (Map<Object, T>) redisTemplate.opsForHash().entries(tenantKey(key));
    }

    @Override
    public Boolean hSetAll(String key, Map<String, T> map, long time) {
        redisTemplate.opsForHash().putAll(tenantKey(key),map);
        return expire(tenantKey(key),time);
    }

    @Override
    public void hSetAll(String key, Map<String, T> map) {
        redisTemplate.opsForHash().putAll(tenantKey(key),map);
    }

    @Override
    public void hDel(String key, Object... hashKey) {
        redisTemplate.opsForHash().delete(tenantKey(key), hashKey);
    }

    @Override
    public Boolean hHasKey(String key, String hashKey) {
        return redisTemplate.opsForHash().hasKey(tenantKey(key), hashKey);
    }

    @Override
    public Long hIncr(String key, String hashKey, Long delta) {
        return redisTemplate.opsForHash().increment(tenantKey(key), hashKey, delta);
    }

    @Override
    public Long hDecr(String key, String hashKey, Long delta) {
        return redisTemplate.opsForHash().increment(tenantKey(key), hashKey, -delta);
    }

    @Override
    public Set<T> sMembers(String key) {
        return redisTemplate.opsForSet().members(tenantKey(key));
    }

    @Override
    public Long sAdd(String key, T... values) {
        return redisTemplate.opsForSet().add(tenantKey(key), values);
    }

    @Override
    public Long sAdd(String key, long time, T... values) {
        Long count = redisTemplate.opsForSet().add(tenantKey(key), values);
        expire(key, time);
        return count;
    }

    @Override
    public Boolean sIsMember(String key, T value) {
        return redisTemplate.opsForSet().isMember(tenantKey(key), value);
    }

    @Override
    public Long sSize(String key) {
        return redisTemplate.opsForSet().size(tenantKey(key));
    }

    @Override
    public Long sRemove(String key, T... values) {
        return redisTemplate.opsForSet().remove(tenantKey(key), values);
    }

    @Override
    public List<T> lRange(String key, long start, long end) {
        return redisTemplate.opsForList().range(tenantKey(key), start, end);
    }

    @Override
    public Long lSize(String key) {
        return redisTemplate.opsForList().size(tenantKey(key));
    }

    @Override
    public T lIndex(String key, long index) {
        return redisTemplate.opsForList().index(tenantKey(key), index);
    }

    @Override
    public Long lPush(String key, T value) {
        return redisTemplate.opsForList().rightPush(tenantKey(key), value);
    }

    @Override
    public Long lPush(String key, T value, long time) {
        Long index = redisTemplate.opsForList().rightPush(tenantKey(key), value);
        expire(key, time);
        return index;
    }

    @Override
    public Long lPushAll(String key, T... values) {
        return redisTemplate.opsForList().rightPushAll(tenantKey(key), values);
    }

    @Override
    public Long lPushAll(String key, Long time, T... values) {
        Long count = redisTemplate.opsForList().rightPushAll(tenantKey(key), values);
        expire(key, time);
        return count;
    }

    @Override
    public Long lRemove(String key, long count, T value) {
        return redisTemplate.opsForList().remove(tenantKey(key), count, value);
    }

}
