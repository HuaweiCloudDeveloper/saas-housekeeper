/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package com.huawei.housekeeper.exception;

import com.huawei.housekeeper.constants.BaseCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 参数校验异常处理
 */
@Setter
@Getter
public class ArgumentException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    // 默认code
    private int code;

    public ArgumentException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public ArgumentException(int code, String message) {
        this(code, message, null);
    }

    public ArgumentException(BaseCode code, Throwable cause) {
        this(code.getCode(), code.getMessage(), cause);
    }

    public ArgumentException(BaseCode code) {
        this(code, null);
    }
}