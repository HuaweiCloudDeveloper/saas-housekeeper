/**
 * 服务对象
 */
export interface Service {
    id: number;
    createTime: string;
    createdBy: string | null;
    displayPrice: number;
    serviceDesc: string;
    serviceName: string;
    servieStatus: string;
    specificationName: string;
    updatedTime: string;
    updatedBy: string | null;
}

export interface Spec {
    specId: number;
    name: string;
    options: Array<SpecOption>;
}

export interface SpecOption {
    optionId: number;
    optionName: string;
    selectionId: number | null;
    specName: string | null;
}

export interface Sku {
    price: number;
    skuId: number;
    selections: Array<SkuSelection>;
}

export interface SkuSelection {
    optionId: number;
    optionName: string;
    selectionId: number;
    specName: string;
}
