
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.service;

import com.huawei.housekeeper.controller.request.GetSkuDetailBySkuIdDto;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

/**
 * 服务详情调用
 *
 * @author lWX1128557
 * @since 2022-03-01
 */
@Component
@FeignClient(value = "saas-housekeeper-publish")
public interface PublishService {
    /**
     * 调用服务中心的查询服务
     *
     * @param getSkuDetailBySkuIdDto 根据skuId查询服务详情
     * @return 服务详情
     */
    @PostMapping(value = "/housekeeper/getSkuDetailBySkuId")
    String getSkuDetailBySkuId(@Valid @RequestBody GetSkuDetailBySkuIdDto getSkuDetailBySkuIdDto);
}
