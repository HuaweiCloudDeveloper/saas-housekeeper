/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 查询服务详情Dto
 *
 * @author lWX1128557
 * @since 2022-02-23
 */
@Setter
@Getter
@ApiModel(value = "根据skuId拿到服务详细描述")
public class GetSkuDetailBySkuIdDto {
    @NotNull
    @Min(value = 1L, message = "最小值：1")
    @ApiModelProperty(value = "服务ID", required = true)
    private Integer skuId;
}
