/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 雇员任务查询
 *
 * @author jwx1116205
 * @since 2022-03-02
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("雇员任务查询响应vo")
public class TaskPageListVo {
    @ApiModelProperty("任务id")
    private Long id;

    @ApiModelProperty("服务时间")
    private Date appointmentTime;

    @ApiModelProperty("地址")
    private String address;

    @ApiModelProperty("服务名称")
    private String serviceName;

    @ApiModelProperty("任务状态")
    private String taskStatus;

    @ApiModelProperty("更新时间")
    private Date updatedTime;

    @ApiModelProperty("version")
    private String version;

    @ApiModelProperty("佣金,可通过支付金额得出")
    private BigDecimal salary;

    @ApiModelProperty("订单数量")
    private Integer amount;

    @ApiModelProperty("服务选集")
    private String serviceDetail;

    @ApiModelProperty("订单备注")
    private String remark;
    @ApiModelProperty("图片id")
    private String imgSrc;

}