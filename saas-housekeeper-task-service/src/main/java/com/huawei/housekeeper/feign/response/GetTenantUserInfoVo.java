/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.feign.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 查询用户响应vo
 *
 * @author cwx1109128
 * @since 2022-09-06
 */
@Getter
@Setter
@ApiModel("租户查询用户信息列表vo")
public class GetTenantUserInfoVo {
    @ApiModelProperty("用户id")
    private String userId;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("手机号")
    private String phoneNo;
}
