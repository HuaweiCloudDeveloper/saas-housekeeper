import { i18n, t } from "@/i18n";
import { computed, ComputedRef } from 'vue'

export const errorCodes = [
    //10 通用
    "100001", // 没有权限"
    "100002", // 系统异常"
    "100003", // 未知异常"
    "100004", // 参数非法"
    "100005", // 参数为空"
    "100006", // 消息接收失败"
    "100007", // 查询结果为空！"
    //11 用户
    "110001", // 用户名或者密码错误"
    "110002", // 用户名重复!"
    "110003", // 不能删除当前用户"
    "110004", // 用户不存在"
    "110006", // token过期"
    "110007", // token错误"
    //12 租户
    "120001", // 名字/域名/企业信用码重复"
    "120002", // 路由失败"
    "120003", // 缺失租户标识!"
    "120004", // 租户状态更改有误"
    "120005", // 租户数据库重复"
    "120006", // 邮箱验证码过期"
    "120007", // 验证码错误"
    "120008", // 发起注册失败！"
    "120009", // 邮箱已被注册"
    //13 服务
    "130000", // 服务名重复!"
    "130001", // 服务不存在!"
    "130002", // 服务规格已存在!"
    "130003", // 服务规格不存在!"
    "130004", // 该skuId没有对应的数据!"
    "130005", // 服务选集重复!"
    "130006", // 规格选项为空!"
    "130007", // 服务选集为空!"
    //14 订单
    "140001", // 取消操作，任务的状态必须是已接单！!"
    "140002", // 任务完成操作，任务的状态必须是已接单！"
    "140003", // 抢单操作，任务的状态必须是待抢单！"
    "140004", // 任务操作非法!"
    "140005", // 订单任务已取消!"
    "140006", // 任务不存在!"
    "140007", // 订单任务已存在!"
    "140009", // 抢单失败!
];

export type ErrorCodeTuple = typeof errorCodes;
export type ErrorCode = ErrorCodeTuple[number]

export const ErrorCodeMap = errorCodes.reduce((map, item) => {
    map[item] = computed(() => t(`responseMsg.codes.${item}`))
    return map
}, {} as Record<ErrorCode, ComputedRef<string>>)

/**
 * Check if all error message in locale messages
 * 检查错误码是否设置了 locale，防止漏写词条
 */
function checkCodesAndLocaleMessageSetting() {
    const codesMap = i18n.global.getLocaleMessage(i18n.global.locale.value).responseMsg.codes
    const lackCodes: ErrorCode[] = [];
    errorCodes.forEach(errorCode => {
        if (codesMap[errorCode] === undefined || codesMap[errorCode] === null) {
            lackCodes.push(errorCode);
        }
    })
    if (lackCodes.length) {
        const errorMsg = `In locale messages 'responseMsg.codes', error ${lackCodes.length === 1 ? 'code' : 'codes'} [${lackCodes}] missing.`
        console.error(errorMsg);
    }
}
checkCodesAndLocaleMessageSetting()
