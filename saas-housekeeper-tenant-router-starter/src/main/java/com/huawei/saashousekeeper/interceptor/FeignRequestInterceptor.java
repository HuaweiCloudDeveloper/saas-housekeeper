/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.interceptor;

import com.huawei.saashousekeeper.context.TenantContext;
import com.huawei.saashousekeeper.constants.Constants;

import feign.RequestInterceptor;
import feign.RequestTemplate;

/**
 * feign调用请求头添加租户标识
 *
 * @since 2022-02-28
 */
public class FeignRequestInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header(Constants.TENANT_DOMAIN, TenantContext.getDomain());
    }
}
