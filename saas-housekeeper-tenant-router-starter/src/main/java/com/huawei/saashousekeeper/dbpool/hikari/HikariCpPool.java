/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.dbpool.hikari;

import com.huawei.saashousekeeper.constants.DbPoolEnum;
import com.huawei.saashousekeeper.customedprocessor.PoolRefreshProcessor;
import com.huawei.saashousekeeper.dbpool.JdbcPool;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.zaxxer.hikari.HikariConfig;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.log4j.Log4j2;

/**
 * HikariCp参数配置
 *
 * @author TaoYu
 * @since 2.4.1
 */
@Data()
@EqualsAndHashCode(callSuper = false)
@Log4j2
@JsonIgnoreProperties
public class HikariCpPool extends HikariConfig implements JdbcPool {

    @Override
    public boolean refresh(JdbcPool jdbcPool, PoolRefreshProcessor processor) {
        return processor != null && processor.check(this, jdbcPool);
    }

    @Override
    public String getPoolName() {
        return DbPoolEnum.POOL_HIKARI.getName();
    }
}
