/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.config.dynamicdatasource;

import com.huawei.saashousekeeper.exception.CloseNotSupportException;
import com.huawei.saashousekeeper.properties.DataSourceProperty;
import com.huawei.saashousekeeper.utils.StringUtil;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Closeable;
import java.io.IOException;

import javax.sql.DataSource;

/**
 * 动态数据源快照
 *
 * @author lWX1156935
 * @since 2022/4/22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SnapshotDataSource {
    private DataSourceProperty property;

    private DataSource source;

    private boolean closed = false;

    /**
     * 关闭数据源
     *
     * @return 关闭结果
     * @throws IOException 异常
     */
    public void close() throws IOException {
        if (source == null) {
            closed = true;
            return;
        }
        if (!(source instanceof Closeable)) {
            throw new CloseNotSupportException(StringUtil.getUri(property != null ? property.getUrl() : "")
                + "  The connection pool of dataSource {} does not inherit the closeable interface. Therefore, the connection pool cannot be closed");
        }
        ((Closeable) source).close();
        closed = true;
    }
}
