package com.huawei.housekeeper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * 启动类
 *
 * @author y00464350
 * @since 2022-02-11
 */
@EnableConfigServer
@SpringBootApplication(scanBasePackages = {"com.huawei.**"})
public class SaasHouseKeeperConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(SaasHouseKeeperConfigApplication.class, args);
    }
}
