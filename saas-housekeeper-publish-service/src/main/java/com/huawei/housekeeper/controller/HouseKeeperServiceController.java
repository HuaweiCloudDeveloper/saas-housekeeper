/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller;

import com.huawei.housekeeper.result.ListRes;
import com.huawei.housekeeper.result.Result;
import com.huawei.housekeeper.controller.request.CreateServiceDto;
import com.huawei.housekeeper.controller.request.CreateServiceSelectionDto;
import com.huawei.housekeeper.controller.request.CreateServiceSpecificationDto;
import com.huawei.housekeeper.controller.request.DeleteServiceDto;
import com.huawei.housekeeper.controller.request.DeleteServiceSkuDto;
import com.huawei.housekeeper.controller.request.DeleteSpecificationDto;
import com.huawei.housekeeper.controller.request.GetServiceDto;
import com.huawei.housekeeper.controller.request.GetServiceListDto;
import com.huawei.housekeeper.controller.request.GetSkuDetailBySkuIdDto;
import com.huawei.housekeeper.controller.request.UpdateSeriviceSkuDto;
import com.huawei.housekeeper.controller.request.UpdateServiceDto;
import com.huawei.housekeeper.controller.response.HouseKeeperServiceListVo;
import com.huawei.housekeeper.controller.response.HouseKeeperServiceVo;
import com.huawei.housekeeper.controller.response.ServiceInfoRespVo;
import com.huawei.housekeeper.controller.response.ServiceSkuInfoRespVo;
import com.huawei.housekeeper.service.IHouseKeeperService;
import com.huawei.housekeeper.service.IHouseKeeperServiceSkuService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 功能描述 家政服务接口
 *
 * @author jWX1116205
 * @since 2022-01-17
 */
@Api(tags = "家政服务接口") // 作用在模块API类上，对API模块进行说明
@RestController
@Validated
@RequestMapping("/housekeeper")
public class HouseKeeperServiceController {

    @Autowired
    IHouseKeeperService housekeeperService;

    @Autowired
    IHouseKeeperServiceSkuService houseKeeperServiceSkuService;

    @ApiOperation("服务添加")
    @PostMapping("/createService")
    public Result<Long> createService(@Valid @RequestBody CreateServiceDto createServiceDto,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(housekeeperService.createHousekeeperService(createServiceDto));
    }

    @ApiOperation("服务修改")
    @PostMapping("/updateService")
    public Result<Long> updateService(@Valid @RequestBody UpdateServiceDto updateServiceDto,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(housekeeperService.updateHousekeeperService(updateServiceDto));
    }

    @ApiOperation("服务删除")
    @DeleteMapping("/deleteService")
    public Result<Long> deleteService(@Valid @RequestBody DeleteServiceDto deleteServiceDto,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(housekeeperService.deleteHousekeeperService(deleteServiceDto));
    }

    @ApiOperation("服务添加规格")
    @PostMapping("/createServiceSpecification")
    public Result<Long> createServiceSpecification(
        @Valid @RequestBody CreateServiceSpecificationDto createServiceSpecificationDto,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(
            houseKeeperServiceSkuService.createHousekeeperServiceSpecification(createServiceSpecificationDto));
    }

    @ApiOperation("添加服务选项")
    @PostMapping("/createServiceSelection")
    public Result<String> createServiceSelection(
        @Valid @RequestBody CreateServiceSelectionDto createServiceSelectionDto,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(
            houseKeeperServiceSkuService.createHousekeeperServiceSelection(createServiceSelectionDto));
    }

    @ApiOperation("编辑服务选项")
    @PostMapping("/updateServiceSelection")
    public Result<Long> updateServiceSelection(@Valid @RequestBody UpdateSeriviceSkuDto updateSeriviceSkuDto,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(houseKeeperServiceSkuService.updateSeriviceSku(updateSeriviceSkuDto));
    }

    @ApiOperation("删除服务规格")
    @DeleteMapping("/deleteSpecification")
    public Result<Long> deleteSpecification(@Valid @RequestBody DeleteSpecificationDto deleteSpecificationDto,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(
            houseKeeperServiceSkuService.deleteHousekeeperServiceSpecification(deleteSpecificationDto));
    }

    @ApiOperation("删除SKU")
    @DeleteMapping("/deleteServiceSkuBySkuId")
    public Result<Long> deleteHousekeeperServiceSkuBySkuId(@Valid @RequestBody DeleteServiceSkuDto deleteServiceSkuDto,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(
            houseKeeperServiceSkuService.deleteHousekeeperServiceSkuBySkuId(deleteServiceSkuDto));
    }

    @ApiOperation("服务明细查询")
    @PostMapping("/queryServiceById")
    public Result<HouseKeeperServiceVo> getHousekeeperServiceVoById(@Valid @RequestBody GetServiceDto getServiceListReq,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(housekeeperService.queryHousekeeperServiceVoById(getServiceListReq));
    }

    @ApiOperation("单个服务查询")
    @PostMapping("/getServiceById")
    public Result<ServiceInfoRespVo> getServiceById(@Valid @RequestBody GetServiceDto getServiceListReq,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(housekeeperService.getHousekeeperServiceVoById(getServiceListReq));
    }

    @ApiOperation("服务管理列表查询")
    @PostMapping("/getServiceList")
    public Result<ListRes<HouseKeeperServiceListVo>> getServiceList(
        @Valid @RequestBody GetServiceListDto getServiceListReq,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(housekeeperService.getHousekeeperServicePageByParam(getServiceListReq));
    }

    @ApiOperation("服务首页查询")
    @PostMapping("/getServiceIndexList")
    public Result<ListRes<HouseKeeperServiceListVo>> getServiceIndexList(
        @Valid @RequestBody GetServiceListDto getServiceListReq,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(housekeeperService.getHousekeeperServiceIndexPageByParam(getServiceListReq));
    }

    @ApiOperation("根据skuid拿到服务详细描述")
    @PostMapping("/getSkuDetailBySkuId")
    public Result<ServiceSkuInfoRespVo> getSkuDetailBySkuId(
        @Valid @RequestBody GetSkuDetailBySkuIdDto getSkuDetailBySkuIdDto,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(houseKeeperServiceSkuService.getSkuDetailBySkuId(getSkuDetailBySkuIdDto));
    }
}