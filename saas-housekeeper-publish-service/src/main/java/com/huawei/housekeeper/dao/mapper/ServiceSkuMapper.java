/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.dao.mapper;

import com.huawei.housekeeper.dao.entity.ServiceSku;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * 可选服务表(SKU)Mapper接口
 *
 * @author jwx1116205
 * @since 2022-03-03
 */
@Mapper
public interface ServiceSkuMapper extends BaseMapper<ServiceSku> {
    int deleteByServcieId(Long serviceId, String updatedBy);
}