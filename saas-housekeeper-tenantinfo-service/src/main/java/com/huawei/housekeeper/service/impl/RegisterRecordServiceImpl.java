package com.huawei.housekeeper.service.impl;

import com.huawei.housekeeper.utils.RandomUtils;
import com.huawei.housekeeper.controller.request.GetIdentifyCodeDto;
import com.huawei.housekeeper.dao.entities.TenantRegisterRecord;
import com.huawei.housekeeper.dao.mapper.RegisterRecordMapper;
import com.huawei.housekeeper.property.EmailProperties;
import com.huawei.housekeeper.service.MailService;
import com.huawei.housekeeper.service.RegisterRecordService;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 功能描述
 *
 * @since 2022-10-20
 */
@Service
public class RegisterRecordServiceImpl extends ServiceImpl<RegisterRecordMapper, TenantRegisterRecord> implements RegisterRecordService {


    @Autowired
    private MailService mailService;


    @Autowired
    private EmailProperties emailProperties;

    /**
     * 调用验证码
     *
     * @param getIdentifyCodeDto 调用验证码dto
     * @return 随机码
     */
    @Override
    public String getIdentifyCode(GetIdentifyCodeDto getIdentifyCodeDto) {
        Integer code;
        try {
            code = RandomUtils.code();
            TenantRegisterRecord tenantRegisterRecord = new TenantRegisterRecord();
            tenantRegisterRecord.setEmail(getIdentifyCodeDto.getEmail());
            tenantRegisterRecord.setDeleteFlag(0L);
            save(tenantRegisterRecord);
            if (emailProperties.getEnable()) {
                mailService.sendSimpleMail(getIdentifyCodeDto.getEmail(), null, "注册验证码", emailProperties.getContent()
                        + emailProperties.getWebsite() + "?code=" + code + "&id=" + tenantRegisterRecord.getId());
                return "邮件发送成功，请前往邮箱查看邮件";
            }
            return emailProperties.getContent()
                    + emailProperties.getWebsite() + "?code=" + code + "&id=" + tenantRegisterRecord.getId();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }
}