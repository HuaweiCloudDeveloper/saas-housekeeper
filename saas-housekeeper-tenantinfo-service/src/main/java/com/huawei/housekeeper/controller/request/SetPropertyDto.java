/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import lombok.Getter;
import lombok.Setter;

/**
 * 增加租户路由配置
 *
 * @author y00464350
 * @since 2022-03-24
 */
@Getter
@Setter
public class SetPropertyDto {
    /**
     * 属性 key
     */
    private String key;

    /**
     * 值 value
     */
    private String value;

    /**
     * 客户端服务名
     */
    private String application;

    /**
     * 环境
     */
    private String profile;

    /**
     * 分支
     */
    private String label;
}
