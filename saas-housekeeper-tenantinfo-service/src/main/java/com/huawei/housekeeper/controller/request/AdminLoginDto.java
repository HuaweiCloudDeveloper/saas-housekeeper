/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * 超级管理员登录Dto
 *
 * @author lWX1128557
 * @since 2022-03-03
 */
@ApiModel(value = "管理员登录")
@Getter
@Setter
public class AdminLoginDto {
    @NotBlank
    @Length(max = 32, message = "长度范围：32")
    @ApiModelProperty(value = "姓名")
    private String name;

    @NotBlank
    @Length(max = 64, message = "长度范围：64")
    @ApiModelProperty(value = "密码")
    private String password;
}
