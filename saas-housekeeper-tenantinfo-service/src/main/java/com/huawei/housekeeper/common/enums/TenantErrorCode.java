/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.common.enums;



import com.huawei.housekeeper.constants.BaseCode;
import lombok.Getter;

/**
 * 错误码枚举
 *
 * @author lWX1128557
 * @since 2022-03-03
 */
@Getter
public enum TenantErrorCode implements BaseCode {
    /**
     * 租户注册重复提示
     */
    TENANT_REPEAT(660001, "名字/域名/企业信用码重复"),

    /**
     * 租户没有数据源
     */
    TENANT_NULL(660002, "租户数据源为空"),

    /**
     * 租户状态重复
     */
    STATUS_ERROR(660003, "租户状态更改有误"),

    /**
     * 租户数据库重复
     */
    DATABASE_REPEAT(660004,"租户数据库重复");

    private int code;

    private String message;

    TenantErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
