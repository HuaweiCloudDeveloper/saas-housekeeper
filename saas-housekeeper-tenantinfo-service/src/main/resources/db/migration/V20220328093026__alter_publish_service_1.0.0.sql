DROP TABLE IF EXISTS `t_customization`;

CREATE TABLE `t_customization`
(
    `config_id`  bigint NOT NULL AUTO_INCREMENT COMMENT '配置ID',
    `style_flag` tinyint                                                DEFAULT NULL COMMENT '主题标识',
    `store_name` varchar(50)                                            DEFAULT NULL COMMENT '店名',
    `tenant_id`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '租户标识',
    PRIMARY KEY (`config_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 18
  DEFAULT CHARSET = utf8 COMMENT ='租户配置表';

/*Table structure for table `t_housekeeper_service` */

DROP TABLE IF EXISTS `t_housekeeper_service`;

CREATE TABLE `t_housekeeper_service`
(
    `id`            bigint NOT NULL AUTO_INCREMENT COMMENT '服务ID',
    `service_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '服务名称',
    `service_desc`  varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '服务描述',
    `img_src`       varchar(500)                                            DEFAULT NULL COMMENT '图片地址',
    `created_by`    varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT NULL COMMENT '创建人',
    `created_time`  datetime                                                DEFAULT NULL COMMENT '创建时间',
    `updated_by`    varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT NULL COMMENT '更新人',
    `updated_time`  datetime                                                DEFAULT NULL COMMENT '更新时间',
    `servie_status` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci   DEFAULT NULL COMMENT '服务状态',
    `delete_flag`   varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci   DEFAULT '0' COMMENT '删除标志',
    `revision`      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT NULL COMMENT '乐观锁',
    `display_price` decimal(24, 6)                                          DEFAULT NULL COMMENT '服务展示价格',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1402556435
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT COMMENT ='服务主表';

/*Table structure for table `t_service_option` */

DROP TABLE IF EXISTS `t_service_option`;

CREATE TABLE `t_service_option`
(
    `id`               bigint NOT NULL AUTO_INCREMENT COMMENT '选项ID',
    `specification_id` bigint NOT NULL COMMENT '规格ID',
    `name`             varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '选项名称',
    `created_by`       varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT NULL COMMENT '创建人',
    `created_time`     datetime                                                DEFAULT NULL COMMENT '创建时间',
    `updated_by`       varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT NULL COMMENT '更新人',
    `updated_time`     datetime                                                DEFAULT NULL COMMENT '更新时间',
    `revision`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT NULL COMMENT '乐观锁',
    `delete_flag`      varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci   DEFAULT '0' COMMENT '删除标志',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 155
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT COMMENT ='规格选项表';

/*Table structure for table `t_service_selection` */

DROP TABLE IF EXISTS `t_service_selection`;

CREATE TABLE `t_service_selection`
(
    `id`           bigint NOT NULL AUTO_INCREMENT COMMENT '选集ID',
    `option_id`    bigint                                                 DEFAULT NULL COMMENT '选项ID',
    `sku_id`       bigint                                                 DEFAULT NULL COMMENT 'SKU_ID',
    `created_by`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
    `created_time` datetime                                               DEFAULT NULL COMMENT '创建时间',
    `updated_by`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
    `updated_time` datetime                                               DEFAULT NULL COMMENT '更新时间',
    `revision`     varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '乐观锁',
    `delete_flag`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT '0' COMMENT '删除标志',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 131
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT COMMENT ='选集表';

/*Table structure for table `t_service_sku` */

DROP TABLE IF EXISTS `t_service_sku`;

CREATE TABLE `t_service_sku`
(
    `id`           int NOT NULL AUTO_INCREMENT COMMENT '可选服务ID',
    `service_id`   int                                                    DEFAULT NULL COMMENT '服务ID',
    `price`        decimal(24, 6)                                         DEFAULT NULL COMMENT '价格',
    `created_by`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
    `created_time` datetime                                               DEFAULT NULL COMMENT '创建时间',
    `updated_by`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
    `updated_time` datetime                                               DEFAULT NULL COMMENT '更新时间',
    `revision`     varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '乐观锁',
    `delete_flag`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT '0' COMMENT '删除标志',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 68
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT COMMENT ='可选服务表(SKU)';

/*Table structure for table `t_service_specification` */

DROP TABLE IF EXISTS `t_service_specification`;

CREATE TABLE `t_service_specification`
(
    `id`           bigint NOT NULL AUTO_INCREMENT COMMENT '规格ID',
    `service_id`   bigint                                                  DEFAULT NULL COMMENT '服务ID',
    `name`         varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '规格名称',
    `created_by`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT NULL COMMENT '创建人',
    `created_time` datetime                                                DEFAULT NULL COMMENT '创建时间',
    `updated_by`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT NULL COMMENT '更新人',
    `updated_time` datetime                                                DEFAULT NULL COMMENT '更新时间',
    `revision`     varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT NULL COMMENT '乐观锁',
    `delete_flag`  varchar(1)                                              DEFAULT '0' COMMENT '删除标志',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 810397703
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT COMMENT ='规格表';

/*Table structure for table `t_task` */

DROP TABLE IF EXISTS `t_task`;

CREATE TABLE `t_task`
(
    `id`               bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `customer_id`      int                                                    DEFAULT NULL COMMENT '顾客id;用户中心上下文',
    `order_id`         int                                                    DEFAULT NULL COMMENT '订单id;订单中心上下文',
    `customer_name`    varchar(255)                                           DEFAULT NULL COMMENT '顾客名称',
    `customer_phone`   varchar(255)                                           DEFAULT NULL COMMENT '电话号码',
    `appointment_time` datetime                                               DEFAULT NULL COMMENT '服务时间',
    `address`          varchar(255)                                           DEFAULT NULL COMMENT '地址',
    `service_name`     varchar(255)                                           DEFAULT NULL COMMENT '服务名称;订单中心推送',
    `service_detail`   varchar(255)                                           DEFAULT NULL COMMENT '服务细节;订单中心推送',
    `salary`           decimal(24, 6)                                         DEFAULT NULL COMMENT '佣金,可通过支付金额得出',
    `employee_name`    varchar(255)                                           DEFAULT NULL COMMENT '雇佣名称',
    `amount`           int                                                    DEFAULT NULL COMMENT '订单数量',
    `employee_id`      varchar(32)                                            DEFAULT NULL COMMENT '雇佣id',
    `task_status`      varchar(255)                                           DEFAULT NULL COMMENT '任务状态',
    `created_by`       varchar(90)                                            DEFAULT NULL COMMENT '创建人',
    `created_time`     datetime                                               DEFAULT NULL COMMENT '创建时间',
    `updated_by`       varchar(90)                                            DEFAULT NULL COMMENT '更新人',
    `updated_time`     datetime                                               DEFAULT NULL COMMENT '更新时间',
    `delete_flag`      varchar(1)                                             DEFAULT '0' COMMENT '删除标志',
    `version`          varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'UUID()' COMMENT '乐观锁',
    `remark`           varchar(255)                                           DEFAULT NULL COMMENT '订单备注',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 5043
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT COMMENT ='任务表';